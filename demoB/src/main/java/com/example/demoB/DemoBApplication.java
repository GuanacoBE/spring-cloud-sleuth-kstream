package com.example.demoB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.cloud.stream.binder.kafka.streams.annotations.KafkaStreamsProcessor;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;

@SpringBootApplication
@EnableBinding(KafkaStreamsProcessor.class)
public class DemoBApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBApplication.class, args);
	}

    @StreamListener("input")
    @SendTo("output")
	public KStream<String, String> process(KStream<String, String> input) {
		return input.map((k,v) -> new KeyValue<>(k, v.toUpperCase() + " service B"));
	}

}
